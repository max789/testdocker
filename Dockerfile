FROM java:8
EXPOSE 8080
ADD /build/libs/docker.jar docker.jar
ENTRYPOINT ["java","-jar","docker.jar"]

